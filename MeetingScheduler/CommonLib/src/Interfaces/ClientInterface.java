package Interfaces;

import Meetings.MeetingEvent;
import Meetings.MeetingEventInfo;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface responsible for user interaction and communication with the server
 */
public interface ClientInterface extends Remote
{
    /**
     * Notify the user that a new meeting has been created
     * @param meetingInfo
     * @throws RemoteException
     */
    void notifyNewEvent(MeetingEventInfo meetingInfo) throws RemoteException;
    /**
     * Notify the client that a meeting has ended and shows its information
     * @param meeting
     * @throws RemoteException
     */
    void notifyEndOfEvent(MeetingEvent meeting) throws RemoteException;
}
