package Interfaces;

import Meetings.MeetingEvent;
import Requests.MeetingInfoFeedback;
import Requests.MeetingInfoRequest;
import Requests.MeetingInterestRequest;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.PublicKey;

/**
 * Interface responsible for receiving requests and notifying clients
 */
public interface ServerInterface extends Remote {
    /**
     * Register a new client that connected to the server
     * @param name
     * @param publicKey
     * @param remoteObject
     * @return
     * @throws RemoteException
     */
    boolean registerClient(String name, PublicKey publicKey, ClientInterface remoteObject) throws RemoteException;
    /**
     * Request to register a new meeting
     * @param meetingEvent
     * @return
     * @throws RemoteException
     */
    boolean registerMeeting(MeetingEvent meetingEvent) throws RemoteException;
    /**
     * Request to view information from a meeting
     * @param meetingInfoRequest
     * @return
     * @throws Exception
     */
    MeetingInfoFeedback viewMeetingInfo(MeetingInfoRequest meetingInfoRequest) throws Exception;
    /**
     * Request to record interest from a client to a meeting
     * @param meetingInterestRequest
     * @return
     * @throws RemoteException
     */
    boolean requestMeetingInterest(MeetingInterestRequest meetingInterestRequest) throws RemoteException;

}
