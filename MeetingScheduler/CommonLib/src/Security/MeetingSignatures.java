package Security;

import Requests.MeetingInfoRequest;

import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;

/**
 * Class responsible for signing and verifying signatures of meeting requests
 */
public class MeetingSignatures {
    private MessageSigner signer;

    /**
     * @throws NoSuchAlgorithmException
     */
    public MeetingSignatures() throws NoSuchAlgorithmException {
        signer = new MessageSigner();
    }

    /**
     * Default getter
     * @return MessageSigner
     */
    public MessageSigner getSigner() {
        return signer;
    }

    /**
     * Default setter
     * @param signer
     */
    public void setSigner(MessageSigner signer) {
        this.signer = signer;
    }

    /**
     * Signs a meeting request
     * @param meetingInfo
     * @throws Exception
     */
    public void SignMeetingRequest(MeetingInfoRequest meetingInfo) throws Exception {
        String meetingInfoString = convertMeetingInfoToString(meetingInfo);

        meetingInfo.setSignature(signer.signMessage(meetingInfoString));
    }

    /**
     * Verifies a signature according to a meeting request and public key
     * @param meetingInfo
     * @param publicKey
     * @return
     * @throws Exception
     */
    public boolean verifyMeetingRequestSignature(MeetingInfoRequest meetingInfo, PublicKey publicKey) throws Exception {
        String meetingInfoString = convertMeetingInfoToString(meetingInfo);

        return signer.verifySignedMessage(meetingInfoString, meetingInfo.getSignature(), publicKey);
    }

    /**
     * Convert meeting information to string
     * @param meetingInfo
     * @return
     */
    private String convertMeetingInfoToString(MeetingInfoRequest meetingInfo)
    {
        return meetingInfo.getMeetingName() + meetingInfo.getOwnerRequestName();
    }
}
