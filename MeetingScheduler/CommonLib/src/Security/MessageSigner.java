package Security;

import java.security.*;
import java.security.Signature;

/**
 * Class responsible for signing any messages and verifying signatures
 */
public class MessageSigner {
    private static PublicKey publicKey;
    private static PrivateKey privateKey;
    private static String stringEncoder = "UTF8";
    private static String keyPairAlgorithm = "RSA";
    private static String signatureAlgorithm = "SHA1WithRSA";

    /**
     * @throws NoSuchAlgorithmException
     */
    public MessageSigner() throws NoSuchAlgorithmException
    {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(keyPairAlgorithm);
        kpg.initialize(1024);
        KeyPair kp = kpg.genKeyPair();
        publicKey = kp.getPublic();
        privateKey = kp.getPrivate();
    }

    /**
     * Default getter
     * @return PublicKey
     */
    public PublicKey getPublicKey()
    {
        return this.publicKey;
    }

    /**
     * Default setter
     * @return
     */
    public PrivateKey getPrivateKey()
    {
        return this.privateKey;
    }

    /**
     * Signs a message and returns the signature as an array of bytes
     * @param message
     * @return
     * @throws Exception
     */
    public byte[] signMessage(String message) throws Exception
    {
        byte[] messageBytes = message.getBytes(stringEncoder);;

        Signature sig = Signature.getInstance(signatureAlgorithm);
        sig.initSign(privateKey);
        sig.update(messageBytes);

        return sig.sign();
    }

    /**
     * Verify a signature according to a given message
     * @param message
     * @param signedMessage
     * @param publicKey
     * @return
     * @throws Exception
     */
    public boolean verifySignedMessage(String message, byte[] signedMessage, PublicKey publicKey) throws Exception
    {
        byte[] messageBytes = message.getBytes(stringEncoder);;

        Signature sig = Signature.getInstance(signatureAlgorithm);
        sig.initVerify(publicKey);
        sig.update(messageBytes);

        return sig.verify(signedMessage);
    }
}
