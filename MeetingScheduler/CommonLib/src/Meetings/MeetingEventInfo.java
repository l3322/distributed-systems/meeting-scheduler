package Meetings;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class MeetingEventInfo implements Serializable {
    private String ownerName;
    private String name;
    private String local;
    private Date deadline;
    private Set<Date> availableDates;

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Set<Date> getAvailableDates() {
        return availableDates;
    }

    public void setAvailableDates(Set<Date> availableDates) {
        this.availableDates = availableDates;
    }
}
