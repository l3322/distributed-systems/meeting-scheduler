package Meetings;

import java.io.Serializable;
import java.util.*;

public class MeetingEvent implements Serializable {
    private MeetingEventInfo meetingInfo;
    private HashMap<Date, Integer> dateVotes;
    private HashSet<String> clientVotes;
    private HashSet<String> clientsWithoutVotes;
    private boolean isActive;

    public MeetingEvent(String ownerName,
                        String name,
                        String local,
                        Date deadline,
                        Set<Date> availableDates) {
        meetingInfo = new MeetingEventInfo();
        meetingInfo.setOwnerName(ownerName);
        meetingInfo.setName(name);
        meetingInfo.setLocal(local);
        meetingInfo.setDeadline(deadline);
        meetingInfo.setAvailableDates( availableDates);
        dateVotes = new HashMap<>();
        clientVotes = new HashSet<>();
        clientsWithoutVotes = new HashSet<>();
        isActive = true;
    }

    public String getOwnerName() {
        return meetingInfo.getOwnerName();
    }

    public void setOwnerName(String ownerName) {
        this.meetingInfo.setOwnerName(ownerName);
    }

    public String getName() {
        return meetingInfo.getName();
    }

    public void setName(String name) {
        this.meetingInfo.setName(name);
    }

    public String getLocal() {
        return meetingInfo.getLocal();
    }

    public void setLocal(String local) {
        this.meetingInfo.setLocal(local);
    }

    public Date getDeadline() {
        return meetingInfo.getDeadline();
    }

    public void setDeadline(Date deadline) {
        this.meetingInfo.setDeadline(deadline);
    }

    public HashMap<Date, Integer> getDateVotes() {
        return dateVotes;
    }

    public void setDateVotesCount(HashMap<Date, Integer> dateVotes) {
        this.dateVotes = dateVotes;
    }

    public HashSet<String> getClientVotes() {
        return clientVotes;
    }

    public void setClientVotes(HashSet<String> clientVotes) {
        this.clientVotes = clientVotes;
    }

    public MeetingEventInfo getMeetingInfo() {
        return meetingInfo;
    }

    public void setMeetingInfo(MeetingEventInfo meetingInfo) {
        this.meetingInfo = meetingInfo;
    }

    public void setDateVotes(HashMap<Date, Integer> dateVotes) {
        this.dateVotes = dateVotes;
    }

    public HashSet<String> getClientsWithoutVotes() {
        return clientsWithoutVotes;
    }

    public void setClientsWithoutVotes(HashSet<String> clientsWithoutVotes) {
        this.clientsWithoutVotes = clientsWithoutVotes;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
