package Requests;

import java.io.Serializable;

public class MeetingInfoRequest implements Serializable {
    private String ownerRequestName;
    private String meetingName;
    private byte[] signature;

    public MeetingInfoRequest(String ownerRequestName, String meetingName) {
        this.ownerRequestName = ownerRequestName;
        this.meetingName = meetingName;
    }

    public String getOwnerRequestName() {
        return ownerRequestName;
    }

    public void setOwnerRequestName(String ownerRequestName) {
        this.ownerRequestName = ownerRequestName;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }
}
