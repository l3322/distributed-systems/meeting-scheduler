package Requests;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

public class MeetingInterestRequest implements Serializable {
    private String requestOwnerName;
    private String meetingName;
    private Set<Date> availableTimes;

    public MeetingInterestRequest(String requestOwnerName, String meetingName, Set<Date> availableTimes) {
        this.requestOwnerName = requestOwnerName;
        this.meetingName = meetingName;
        this.availableTimes = availableTimes;
    }

    public String getRequestOwnerName() {
        return requestOwnerName;
    }

    public void setRequestOwnerName(String requestOwnerName) {
        this.requestOwnerName = requestOwnerName;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public Set<Date> getAvailableTimes() {
        return availableTimes;
    }

    public void setAvailableTimes(Set<Date> availableTimes) {
        this.availableTimes = availableTimes;
    }
}
