package Requests;

import Meetings.MeetingEvent;

import java.io.Serializable;

public class MeetingInfoFeedback implements Serializable {
    private OperationResult operationResult;
    private MeetingEvent meetingEvent;

    public MeetingInfoFeedback(OperationResult operationResult, MeetingEvent meetingEvent) {
        this.operationResult = operationResult;
        this.meetingEvent = meetingEvent;
    }

    public OperationResult getOperationResult() {
        return operationResult;
    }

    public void setOperationResult(OperationResult operationResult) {
        this.operationResult = operationResult;
    }

    public MeetingEvent getMeetingEvent() {
        return meetingEvent;
    }

    public void setMeetingEvent(MeetingEvent meetingEvent) {
        this.meetingEvent = meetingEvent;
    }
}
