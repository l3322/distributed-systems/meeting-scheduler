package Requests;

public enum OperationResult {
    SUCCESSFUL, UNAUTHORIZED, INVALID_SIGNATURE, SERVER_ERROR
}
