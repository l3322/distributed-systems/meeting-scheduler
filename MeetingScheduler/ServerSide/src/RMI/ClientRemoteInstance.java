package RMI;

import Interfaces.ClientInterface;

import java.security.PublicKey;

/**
 * Class responsible for storing client instances with its information and remote object
 */
public class ClientRemoteInstance {
    public String name;
    public PublicKey publicKey;
    public ClientInterface remoteObject;

    /**
     * @param _name
     * @param _publicKey
     * @param _remoteObject
     */
    public ClientRemoteInstance(String _name, PublicKey _publicKey, ClientInterface _remoteObject){
        this.setName(_name);
        this.setPublicKey(_publicKey);
        this.setRemoteObject(_remoteObject);
    }

    /**
     * Default getter
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Default setter
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Default getter
     * @return PublicKey
     */
    public PublicKey getPublicKey() {
        return publicKey;
    }

    /**
     * Default setter
     * @param publicKey
     */
    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * Default getter
     * @return ClientInterface
     */
    public ClientInterface getRemoteObject() {
        return remoteObject;
    }

    /**
     * Default setter
     * @param remoteObject
     */
    public void setRemoteObject(ClientInterface remoteObject) {
        this.remoteObject = remoteObject;
    }
}
