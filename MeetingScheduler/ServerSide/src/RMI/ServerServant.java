package RMI;

import Interfaces.ClientInterface;
import Interfaces.ServerInterface;
import Meetings.MeetingEvent;
import Requests.MeetingInfoFeedback;
import Requests.MeetingInfoRequest;
import Requests.MeetingInterestRequest;
import Requests.OperationResult;
import Security.MeetingSignatures;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Server servant class responsible for receiving requests and notifying clients
 */
public class ServerServant extends UnicastRemoteObject implements ServerInterface {
    private HashMap<String, ClientRemoteInstance> clientMap;
    private HashMap<String, MeetingEvent> meetingMap;
    private MeetingSignatures meetingSignatures;

    /**
     * @throws RemoteException
     * @throws NoSuchAlgorithmException
     */
    public ServerServant() throws RemoteException, NoSuchAlgorithmException {
        clientMap = new HashMap<>();
        meetingMap = new HashMap<>();
        meetingSignatures = new MeetingSignatures();
    }

    /**
     * Register a new client that connected to the server
     * @param name
     * @param publicKey
     * @param remoteObject
     * @return
     * @throws RemoteException
     */
    public boolean registerClient(String name, PublicKey publicKey, ClientInterface remoteObject) throws RemoteException
    {
        ClientRemoteInstance clientInstance = new ClientRemoteInstance(name, publicKey, remoteObject);

        clientMap.put(name, clientInstance);

        System.out.println("Client " + name + " connected succesfully.");

        return true;
    }

    /**
     * Request to register a new meeting
     * @param meetingEvent
     * @return
     * @throws RemoteException
     */
    public boolean registerMeeting(MeetingEvent meetingEvent) throws RemoteException
    {
        for(Map.Entry<String, ClientRemoteInstance> client : clientMap.entrySet())
        {
            meetingEvent.getClientsWithoutVotes().add(client.getValue().getName());
            if(client.getValue().getName() != meetingEvent.getOwnerName())
            {
                client.getValue().getRemoteObject().notifyNewEvent(meetingEvent.getMeetingInfo());
            }
        }

        for(Date date : meetingEvent.getMeetingInfo().getAvailableDates())
        {
            meetingEvent.getDateVotes().put(date, 0);
        }

        // Register meeting
        meetingMap.put(meetingEvent.getName(), meetingEvent);

        return true;
    }

    /**
     * Request to view information from a meeting
     * @param meetingInfoRequest
     * @return
     * @throws Exception
     */
    public MeetingInfoFeedback viewMeetingInfo(MeetingInfoRequest meetingInfoRequest) throws Exception
    {
        MeetingEvent meeting = meetingMap.get(meetingInfoRequest.getMeetingName());
        ClientRemoteInstance clientRemoteInstance = clientMap.get(meetingInfoRequest.getOwnerRequestName());

        if(!meeting.getClientVotes().contains(meetingInfoRequest.getOwnerRequestName()))
        {
            return new MeetingInfoFeedback(OperationResult.UNAUTHORIZED, null);
        }

        if(!meetingSignatures.verifyMeetingRequestSignature(meetingInfoRequest, clientRemoteInstance.getPublicKey()))
        {
            return new MeetingInfoFeedback(OperationResult.INVALID_SIGNATURE, null);
        }

        return new MeetingInfoFeedback(OperationResult.SUCCESSFUL, meeting);
    }

    /**
     * Request to record interest from a client to a meeting
     * @param meetingInterestRequest
     * @return
     * @throws RemoteException
     */
    public boolean requestMeetingInterest(MeetingInterestRequest meetingInterestRequest) throws RemoteException
    {
        if (!meetingMap.containsKey(meetingInterestRequest.getMeetingName())) {
            return false;
        }
        MeetingEvent meeting = meetingMap.get(meetingInterestRequest.getMeetingName());

        if(meeting.getClientVotes().contains(meetingInterestRequest.getRequestOwnerName()))
        {
            return false;
        }


        for(Date date : meetingInterestRequest.getAvailableTimes())
        {
            Integer dateVotes = meeting.getDateVotes().get(date);
            meeting.getDateVotes().put(date, dateVotes+1);
        }

        meeting.getClientVotes().add(meetingInterestRequest.getRequestOwnerName());
        meeting.getClientsWithoutVotes().remove(meetingInterestRequest.getRequestOwnerName());

        if(meeting.getClientsWithoutVotes().isEmpty())
        {
            notifyEndOfMeeting(meeting);
        }

        return true;
    }

    /**
     * The server will continously validate meetings and its deadline. Notifies clients if a deadline has been reached.
     * @throws RemoteException
     * @throws InterruptedException
     */
    public void runServerValidations() throws RemoteException, InterruptedException {
        // Looping infinitely here. Not a good practice but easy for testing.
        while(true) {
            for (Map.Entry<String, MeetingEvent> entry : meetingMap.entrySet()) {
                MeetingEvent meeting = entry.getValue();
                if (meeting.isActive()) {
                    if (hasMeetingEnded(meeting)) {
                        notifyEndOfMeeting(meeting);
                    }
                }
            }
            Thread.currentThread().sleep(30000);
        }
    }

    /**
     * Compares two dates in order to identify if a meeting has ended
     * @param meeting
     * @return
     */
    private boolean hasMeetingEnded(MeetingEvent meeting)
    {
        Date currentDate = new Date();
        return currentDate.after(meeting.getDeadline());
    }

    /**
     * Notify all clients that a meeting has ended
     * @param meeting
     * @throws RemoteException
     */
    private void notifyEndOfMeeting(MeetingEvent meeting) throws RemoteException {
        for(String client : meeting.getClientVotes())
        {
            clientMap.get(client).getRemoteObject().notifyEndOfEvent(meeting);
        }
        for(String client : meeting.getClientsWithoutVotes())
        {
            clientMap.get(client).getRemoteObject().notifyEndOfEvent(meeting);
        }
        meeting.setActive(false);
    }
}
