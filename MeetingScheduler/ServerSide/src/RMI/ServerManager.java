package RMI;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;

/**
 * Class responsible for initializing the server communication
 */
public class ServerManager {
    public ServerServant serverServant;

    /**
     * Constructor for the class that automatically initializes the communication
     */
    public ServerManager()
    {
        initializeRMI();
    }

    /**
     * Initializes communication for the server
     */
    public void initializeRMI()
    {
        try
        {
            serverServant = new ServerServant();

            Registry referenceServerName = LocateRegistry.createRegistry(1099);

            try
            {
                referenceServerName.bind("Server", serverServant);

                System.out.println("Server initialization was succesfull... ");

                serverServant.runServerValidations();
            }
            catch(java.rmi.AlreadyBoundException e)
            {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
            catch(RemoteException | NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
    }
}
