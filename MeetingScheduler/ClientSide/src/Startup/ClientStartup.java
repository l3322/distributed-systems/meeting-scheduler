package Startup;

import RMI.ClientManager;

import java.util.Scanner;

public class ClientStartup {
    private static ClientManager clientManager;

    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);

        System.out.print("Enter the client's name: ");
        String clientName= sc.nextLine();

        clientManager = new ClientManager(clientName);
    }
}
