package RMI;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import Interfaces.ServerInterface;
import Interfaces.ClientInterface;
import Meetings.MeetingEvent;
import Meetings.MeetingEventInfo;
import Requests.MeetingInfoFeedback;
import Requests.MeetingInfoRequest;
import Requests.MeetingInterestRequest;
import Requests.OperationResult;
import Security.MeetingSignatures;

/**
 *  Client servant class responsible for user interaction and communication with the server
 */
public class ClientServant extends UnicastRemoteObject implements ClientInterface {
    private String name;
    private ServerInterface serverReference;
    private HashMap<Integer, Runnable> availableInputOptions;
    private HashMap<String, MeetingEventInfo> meetingMap;
    private MeetingSignatures meetingSignatures;

    /**
     * @param _serverReference
     * @param _name
     * @throws RemoteException
     * @throws NoSuchAlgorithmException
     */
    public ClientServant(ServerInterface _serverReference,
                         String				_name) throws RemoteException, NoSuchAlgorithmException {
        this.name = _name;
        serverReference = _serverReference;
        meetingSignatures = new MeetingSignatures();
        meetingMap = new HashMap<>();
        availableInputOptions = new HashMap<>();
        availableInputOptions.put(1, () -> {
            try {
                registerMeeting();
            } catch (RemoteException e) {
                System.out.println("An error occured within the server");
                showGeneralInput();
            }
        });
        availableInputOptions.put(2, () -> {
            try {
                if(!meetingMap.isEmpty())
                {
                    registerMeetingInterest();
                }
                else
                {
                    System.out.println("No events were registered yet");
                    showGeneralInput();
                }
            } catch (RemoteException e) {
                System.out.println("An error occured within the server");
                showGeneralInput();
            }
        });
        availableInputOptions.put(3, () -> {
            try {
                if(!meetingMap.isEmpty())
                {
                requestMeetingInfo();
                }
                else
                {
                    System.out.println("No events were registered yet");
                    showGeneralInput();
                }
            } catch (Exception e) {
                System.out.println("An error occured within the server");
                showGeneralInput();
            }
        });
    }

    /**
     * Notify the user that a new meeting has been created
     * @param meetingInfo
     * @throws RemoteException
     */
    public void notifyNewEvent(MeetingEventInfo meetingInfo) throws RemoteException
    {
        System.out.println();
        System.out.println("New event created with the following information: ");
        System.out.println("Meeting name: " + meetingInfo.getName());
        System.out.println("Local: " + meetingInfo.getLocal());
        System.out.println("Deadline: " + meetingInfo.getDeadline());
        System.out.println("Available times: ");
        List<Date> availableDatesLit = new ArrayList<>(meetingInfo.getAvailableDates());
        Collections.sort(availableDatesLit);
        for(Date time : availableDatesLit)
        {
            System.out.println(time);
        }
        meetingMap.put(meetingInfo.getName(), meetingInfo);
        showGeneralInfoInput();
    }

    /**
     * Shows available input to the user
     */
    public void showGeneralInput()
    {
        Scanner sc= new Scanner(System.in);

        showGeneralInfoInput();

        String clientInput = sc.nextLine();
        try{
            int number = Integer.parseInt(clientInput);
            if(availableInputOptions.containsKey(number))
            {
                availableInputOptions.get(number).run();
            }
            else
            {
                System.out.println("Invalid input");
                showGeneralInput();
            }
        }
        catch (NumberFormatException ex){
            System.out.println("Invalid input");
            showGeneralInput();
        }

    }

    /**
     * Register a new meeting within the server
     * @throws RemoteException
     */
    private void registerMeeting() throws RemoteException
    {
        String eventName;
        String local;
        Set<Date> eventDates = new HashSet<Date>();
        Date deadlineDate = new Date();
        DateFormat df = new SimpleDateFormat("HH:mm dd/MM/yyy", Locale.ENGLISH);
        Scanner sc = new Scanner(System.in);
        System.out.println();

        System.out.print("Enter the name of the event: ");
        eventName = sc.nextLine();

        System.out.print("Enter the local of the event: ");
        local = sc.nextLine();

        boolean validDeadline = false;
        Date currentDate = new Date();
        while(!validDeadline) {
            System.out.println("Enter the deadline of the event in the following format:");
            System.out.println("HH:mm dd/MM/yyyy");
            String deadline = sc.nextLine();
            try
            {
                deadlineDate = df.parse(deadline);
                if(deadlineDate.after(currentDate))
                {
                    validDeadline = true;
                }
                else
                {
                    System.out.println("Deadline should be after the current date");
                }
            } catch (Exception e)
            {
                System.out.println("Invalid format");
            }
        }

        boolean validAvailableTimes = false;
        while(!validAvailableTimes) {
            String date = "";
            if(eventDates.isEmpty()) {
                System.out.println("Enter a time proposal for the event in the following format:");
                System.out.println("HH:mm dd/MM/yyyy");
                date = sc.nextLine();
            }
            else
            {
                System.out.println("Would you like to enter a new time proposal?");
                System.out.println("1 - New time proposal");
                System.out.println("Other - No");
                date = sc.nextLine();
                try {
                    int number = Integer.parseInt(date);
                    if (number != 1) {
                        validAvailableTimes = true;
                    }
                } catch (Exception e) {
                    validAvailableTimes = true;
                }
            }
            if(!validAvailableTimes) {
                try {
                    Date newDate = df.parse(date);
                    if (newDate.after(currentDate)) {
                        eventDates.add(newDate);
                    } else {
                        System.out.println("Time should be after the current time");
                    }
                } catch (Exception e) {
                    System.out.println("Invalid format");
                }
            }
        }

        MeetingEvent meetingEvent = new MeetingEvent(this.name,
                                                     eventName,
                                                     local,
                                                     deadlineDate,
                                                     eventDates);

        System.out.println();

        if(serverReference.registerMeeting(meetingEvent))
        {
            System.out.println("You registered your meeting succesfully");
        }
        else
        {
            System.out.println("An error occured within the server");
        }
        showGeneralInput();
    }

    /**
     * Register interest to a existing meeting in the server
     * @throws RemoteException
     */
    private void registerMeetingInterest() throws RemoteException {
        String eventName = "";
        Set<Date> eventDates = new HashSet<Date>();

        showAvailableMeetingNames();

        Scanner sc= new Scanner(System.in);
        DateFormat df = new SimpleDateFormat("HH:mm dd/MM/yyy", Locale.ENGLISH);
        boolean validEvent = false;
        while(!validEvent) {
            System.out.print("Enter the name of the event: ");
            eventName = sc.nextLine();

            if(meetingMap.containsKey(eventName))
            {
                validEvent = true;
            }
            else
            {
                System.out.println("Invalid event");
            }
        }

        boolean validAvailableTimes = false;
        while(!validAvailableTimes) {
            System.out.println("Enter a time proposal for the event in the following format:");
            System.out.println("HH:mm dd/MM/yyyy");
            String date = sc.nextLine();
            try
            {
                Date newDate = df.parse(date);
                if(meetingMap.get(eventName).getAvailableDates().contains(newDate)) {
                    eventDates.add(newDate);
                }
                else
                {
                    System.out.println("This date was not proposed by the owner of the meeting");
                }
                if(!eventDates.isEmpty()) {
                    System.out.println("Would you like to enter a new time proposal?");
                    System.out.println("1 - New time proposal");
                    System.out.println("Other - No");
                    String input = sc.nextLine();
                    try {
                        int number = Integer.parseInt(input);
                        if (number != 1) {
                            validAvailableTimes = true;
                        }
                    } catch (Exception e) {
                        validAvailableTimes = true;
                    }
                }
            } catch (Exception e)
            {
                System.out.println("Invalid format");
            }
        }

        MeetingInterestRequest meetingInterestRequest = new MeetingInterestRequest(this.name, eventName, eventDates);

        if(serverReference.requestMeetingInterest(meetingInterestRequest))
        {
            System.out.println("You registered your interest succesfully");
        }
        else
        {
            System.out.println("An error occured within the server");
        }

        showGeneralInput();
    }

    /**
     * Requests a meeting info to the server. The meeting needs to exist and signatura must be valid.
     * @throws Exception
     */
    private void requestMeetingInfo() throws Exception {
        String eventName = "";

        Scanner sc= new Scanner(System.in);
        showAvailableMeetingNames();

        boolean validEvent = false;
        while(!validEvent) {
            System.out.print("Enter the name of the event: ");
            eventName = sc.nextLine();

            if(meetingMap.containsKey(eventName))
            {
                validEvent = true;
            }
            else
            {
                System.out.println("Invalid event");
            }
        }

        MeetingInfoRequest meetingInfoRequest = new MeetingInfoRequest(this.name, eventName);
        // Sign the request
        meetingSignatures.SignMeetingRequest(meetingInfoRequest);

        MeetingInfoFeedback meetingInfoFeedback = serverReference.viewMeetingInfo(meetingInfoRequest);

        System.out.println();
        if(meetingInfoFeedback.getOperationResult() == OperationResult.SUCCESSFUL)
        {
            printMeetingInfo(meetingInfoFeedback.getMeetingEvent());
        }
        else if(meetingInfoFeedback.getOperationResult() == OperationResult.UNAUTHORIZED)
        {
            System.out.println("Please vote first before requesting the meeting info");
        }
        else if(meetingInfoFeedback.getOperationResult() == OperationResult.INVALID_SIGNATURE)
        {
            System.out.println("The signature is invalid");
        }

        showGeneralInput();
    }

    public PublicKey getPublicKey(){
        return this.meetingSignatures.getSigner().getPublicKey();
    }

    /**
     * Print the available options to the user
     */
    private void showGeneralInfoInput()
    {
        System.out.println();
        System.out.println("Enter the input according to the following options:");
        System.out.println("1 - Register a new meeting");
        System.out.println("2 - Vote for a meeting");
        System.out.println("3 - Request meeting information");
    }

    /**
     * Notify the client that a meeting has ended and shows its information
     * @param meeting
     * @throws RemoteException
     */
    public void notifyEndOfEvent(MeetingEvent meeting) throws RemoteException
    {
        System.out.println();
        System.out.println("Meeting " + meeting.getName() + " has ended!");
        printMeetingInfo(meeting);
    }

    /**
     * Print information about the meeting
     * @param meetingEvent
     */
    private void printMeetingInfo(MeetingEvent meetingEvent)
    {
        System.out.println("Meeting information: ");
        System.out.println("Meeting name: " + meetingEvent.getMeetingInfo().getName());
        System.out.println("Local: " + meetingEvent.getMeetingInfo().getLocal());
        System.out.println("Deadline: " + meetingEvent.getMeetingInfo().getDeadline());
        System.out.println("Available times and votes: ");
        HashMap<Date, Integer> dateVotes = meetingEvent.getDateVotes();

        for(Map.Entry<Date, Integer> dateVote : dateVotes.entrySet())
        {
            Date date = dateVote.getKey();
            Integer votes = dateVote.getValue();
            System.out.println(date + ": " + votes);
        }
        System.out.println("Clients who have already voted: ");
        HashSet<String> clientVotes = meetingEvent.getClientVotes();

        for(String client : clientVotes)
        {
            System.out.print(client + ", ");
        }
        System.out.println();
        System.out.println("Clients who have not voted yet: ");
        HashSet<String> clientsWithoutVotes = meetingEvent.getClientsWithoutVotes();

        for(String client : clientsWithoutVotes)
        {
            System.out.print(client + ", ");
        }
        System.out.println();
        showGeneralInfoInput();
    }

    private void showAvailableMeetingNames()
    {
        System.out.println("Available meetings:");
        for(Map.Entry<String, MeetingEventInfo> entry : meetingMap.entrySet())
        {
            String meetingName = entry.getKey();
            System.out.print(meetingName + ",");
        }
        System.out.println();
    }
}
