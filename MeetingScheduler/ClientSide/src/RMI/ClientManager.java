package RMI;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.NoSuchAlgorithmException;

import Interfaces.ClientInterface;
import Interfaces.ServerInterface;

/**
 * Manager class for clients to initialize the communication
 */
public class ClientManager {
    private ServerInterface serverReference;
    private ClientServant clientServant;

    /**
     * Constructor that already initializes the RMI communication and initiate interaction with the user
     * @param _name
     */
    public ClientManager(String _name)
    {
        initRMI(_name);
        clientServant.showGeneralInput();
    }

    /**
     * Initializes the RMI communication for the client
     * @param clientName
     */
    public void initRMI(String clientName)
    {
        try
        {
            Registry referenceServerName = LocateRegistry.getRegistry();

            serverReference = (ServerInterface)referenceServerName.lookup("Server");

            clientServant = new ClientServant(serverReference, clientName);

            if(serverReference.registerClient(clientName, clientServant.getPublicKey(), clientServant))
            {
                System.out.println("Connection established with the server.");
            }
            else
                {
                System.out.println("An error occured within the server.");
            }
        }
        catch(AccessException e)
        {
            e.printStackTrace();
        }
        catch (RemoteException e)
        {
            e.printStackTrace();
        }
        catch (NotBoundException e)
        {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
